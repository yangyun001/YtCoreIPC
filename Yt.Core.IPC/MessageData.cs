﻿using System.Collections.Generic;
namespace Yt.Core.IPC
{
    /// <summary>
  /// 通信消息中的远程调用类
  /// </summary>
    internal class MessageData
    { 
        /// <summary>
      /// 接口类型名
      /// </summary>
        public string TypeName { get; set; }
        /// <summary>
        /// 调用接口的方法名
        /// </summary>
        public string MethodName { get; set; }
        /// <summary>
        /// 调用接口的参数列表
        /// </summary>
        public List<object> Args { get; set; }
    }
}
