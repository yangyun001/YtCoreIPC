﻿namespace Yt.Core.IPC
{
	/// <summary>
	/// 消息类型
	/// </summary>
	internal enum MessageType : byte
	{
		/// <summary>
		/// 需要答复的消息
		/// </summary>
		Request,
		/// <summary>
		///不需要答复的消息
		/// </summary>
		NoReplyRequest,
		/// <summary>
		///答复的消息
		/// </summary>
		Response,

		CancellationRequest
	}
}
